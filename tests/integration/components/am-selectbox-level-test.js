import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('am-selectbox-level', 'Integration | Component | am selectbox level', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{am-selectbox-level}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#am-selectbox-level}}
      template block text
    {{/am-selectbox-level}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
