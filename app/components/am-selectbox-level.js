import Ember from 'ember';

export default Ember.Component.extend({
    classNames: ['title', 'data', 'level', 'hideParents', 'parentComponent', 'openedPath', 'selectedData'],
    classNameBindings: ['openedPath', 'selectedData'],

    // Default component properties
    isParent: true,
    level: 0,
    data: [],
    isExpanded: false,
    hideParents: false,
    openedPath: [],
    nodeId: 'selectbox-node-X',
    nextLevelOpenedPath: [],
    selectedData: [],
    isChecked: false,

    initState: function() {
        // Set levels for recursive components
        this.set('currentLevel', parseInt(this.get('currentLevel')));
        this.set('nextLevel', this.get('currentLevel') + 1);

        // If 'only show child options' is enabled, expand all parents
        if (this.get('hideParents') === true) {
            this.set('isExpanded', true);
            this.set('currentLevel', 0);
            this.set('nextLevel', 0);
        }

        // Check if this level is parent or not
        if (typeof(this.get('data').type) === 'undefined') {
            this.set('isParent', true);
        } else {
            this.set('isParent', false);
        }

        // Set unique id for this node
        let nodeId = this.get('title').replace(/[^a-zA-Z0-9]/g, '');
        this.set('nodeId', 'selectbox-node-' + nodeId + '-' + this.get('isParent'));

        // Update checkbox state 
        this.updateCheckboxes();
    }.on('init'),

    updateCheckboxes: function() {
        if (this.get('parentComponent').checkSelected.apply(this, [this.get('data')])) {
            this.set('isChecked', true);
        } else {
            this.set('isChecked', false);
        }
    }.observes('selectedData'),

    observeOpenedPath: async function() {
        // Handle auto open paths
        if (!this.get('hideParents')) {
            try {
                if (this.get('openedPath')[this.get('currentLevel')] == this.get('title')) {
                    await this.set('isExpanded', true);
                    this.set('nextLevelOpenedPath', Ember.copy(this.get('openedPath')));

                    if (this.get('currentLevel') == this.get('openedPath').length - 1) {
                        this.get('parentComponent').send('doneRenderAndJump', this.get('title'));
                    }
                } else {
                    this.set('isExpanded', false);
                }
            } catch (e) { return; }
        }
    }.observes('openedPath'),

    actions: {
        toggleExpand() {
            this.toggleProperty('isExpanded');
        },

        addSelectedData(data, isSelected = false) {
            this.get('parentComponent').send('addSelectedData', data, isSelected);
        },

        viewNodeDetail(node) {
            this.get('parentComponent').send('viewNodeDetail', node);
        },

        hideNodeDetail() {
            this.get('parentComponent').send('hideNodeDetail');
        }
    }
});
