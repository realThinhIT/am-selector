import Ember from 'ember';

export default Ember.Component.extend({
    // Tabs
    TAB_BROWSE: 'browse',
    TAB_SUGGESTIONS: 'suggestions',
    TAB_SUGGESTIONS_UNTAB: 'suggestions_untab',

    // Components bindings by class names
    classNames: [
        'selectedData',
        'updateData'
    ],
    classNameBindings: [
        'selectedData'
    ],

    // Default component properties
    // - Core data
    searchQuery: '',
    selectedData: [], // selected nodes
    browseData: [],
    suggestionsData: [],
    suggestionsUntabData: [],
    // - UI logic data
    processedSelectedData: false,
    updateDataAction: 'suggestionsUpdate', // callback function for routes
    isLoading: true,
    isSuggestionsEnabled: true,
    isSelectPanelVisible: false,
    isDescriptionPanelVisible: false,
    descriptionPanelPosition: 2, // 1 for top, 2 for bot
    openedPath: [],
    ajaxSearchTask: {},

    initState: function() {
        // Select Browse tab as default
        this.set('selectedTab', this.TAB_BROWSE);
    }.on('init'),

    async didInsertElement() {
        // Hide the panel when user click outside the component
        this.$(document).mouseup((e) => {
            let container = this.$(".am-selectbox-container, .am-selectbox-container *");

            if (!container.is(e.target) && container.has(e.target).length === 0) {
                this.set('isSelectPanelVisible', false);
            }
        });

        // Retrieve Browse & Suggestions data
        await this.sendAction('updateDataAction', '', this.TAB_BROWSE, 
            async function (err, data) {
                this.set('browseData', await this.processData(data));
            }.bind(this)
        );

        this.set('isLoading', false);
    },

    actions: {
        // UI handlers
        switchTab(tabName) {
            let oldTab = Ember.copy(this.get('selectedTab'));

            // Switch to a required tab, open up the select panel
            this.set('selectedTab', tabName);
            this.set('isSelectPanelVisible', true);

            // If switch to Browse tab from Suggestions, clear the input
            // If switched from search to suggestions 
            if (
                tabName === this.TAB_BROWSE
                || (oldTab === this.TAB_SUGGESTIONS_UNTAB && tabName === this.TAB_SUGGESTIONS)
            ) {
                this.set('searchQuery', '');
            }

            if (this.get('selectedTab') == this.TAB_SUGGESTIONS && this.get('suggestionsData').length === 0) {
                this.set('isLoading', true);
                this.sendAction('updateDataAction', '', this.TAB_SUGGESTIONS, 
                    async function (err, data) {
                        this.set('suggestionsData', await this.processData(data));
                        this.set('isLoading', false);
                    }.bind(this)
                );
            }
        },

        toggleSelectPanel(isVisible) {
            this.set('isSelectPanelVisible', isVisible || true);
        },

        // Happens everytime user inputs
        onKeyUp() {
            let makeDelay = function (ms) {
                return function (callback) {
                    clearTimeout(this.ajaxSearchTask);
                    this.ajaxSearchTask = setTimeout(callback, ms);
                }.bind(this);
            }.bind(this);

            if (this.get('searchQuery') !== '') {
                // If input value is available, suggests user
                this.set('selectedTab', this.TAB_SUGGESTIONS_UNTAB);
                this.set('isLoading', true);

                // Send action to route in order to update the suggestions
                // func(searchQuery, type, callback(err, data, type))
                let queryString = () => this.sendAction('updateDataAction', this.get('searchQuery'), this.TAB_SUGGESTIONS_UNTAB, 
                    async function (err, data) {
                        this.set('suggestionsUntabData', await this.processData(data));
                        this.set('isLoading', false);
                    }.bind(this)
                );
                makeDelay(300)(queryString);
            } else {
                // If the input is empty, bring up Browse tab
                // Update: only trigger the first time user searches
                this.send('switchTab', this.TAB_SUGGESTIONS);
            }
        },

        // Expand selectbox and jump to the node
        async jumpToPath(paths, index) {
            await this.set('isSelectPanelVisible', true);
            await this.set('openedPath', paths[index]);
            this.set('selectedTab', this.TAB_BROWSE);
        },

        doneRenderAndJump(pathName) {
            // Using jQuery to scroll to the specified category
            let nodeId = pathName.replace(/[^a-zA-Z0-9]/g, '');
            let parentSelector = this.$("#am-selectbox-browse");
            // this.$("#am-selectbox-browse").hide(() => {
                this.$("#am-selectbox-browse").show(() => {
                    // parentSelector.scrollTop(0);

                    parentSelector.animate({
                        scrollTop: 0
                    }, 0, () => {
                        Ember.run.later(() => {
                            let jumpSelector = this.$('#selectbox-node-' + nodeId + '-true');

                            // '#selectbox-node-' + nodeId + '-true'
                            if (jumpSelector.position().top !== 0) {
                                this.$('#am-selectbox-browse').animate({
                                    scrollTop: jumpSelector.position().top
                                }, 'fast');
                            }
                        }, 300);
                    });
                });
            // });
        },

        // Update component data with the selected node
        async addSelectedData(data, isSelected = false) {
            data = Ember.copy(data);

            // NOTE!!!!!!!!!!
            // This is to test in case the suggestions is different from those
            // in the browse json.
            // Remove or comment if it is doesn't working with live data!
            if (this.get('selectedTab') === this.TAB_SUGGESTIONS) {
                data.path.pop();
                data.path = JSON.parse(JSON.stringify(data.path)); // fix the problem displaying on UI
            }

            // If the data is not there, push in the array
            if (isSelected == false) {
                if (!this.checkSelected.apply(this, [data])) {
                    this.get('selectedData').push(data);
                }
            }
            // If the data is selected, unselect it 
            else {
                this.send('deleteSelectedDataById', data.id);
                data = undefined;
            }

            // Reset search query and update the selected options box
            this.set('searchQuery', '');
            this.send('updateSelectedBox', data);

            // Switch to Suggestions tab after search 
            if (this.get('selectedTab') === this.TAB_SUGGESTIONS_UNTAB) {
                this.send('switchTab', this.TAB_SUGGESTIONS);
            }

            // Send notification to the route/ component 
            this.set('selectedData', Ember.copy(this.get('selectedData')));

            // Retrieve new Suggestions data
            // and switch to Suggestions tab
            // this.set('isLoading', true);
            // this.set('isSuggestionsEnabled', true);

            // this.sendAction('updateDataAction', '', this.TAB_SUGGESTIONS, 
            //     async function (err, data) {
            //         this.set('selectedTab', this.TAB_SUGGESTIONS);
            //         this.set('suggestionsData', await this.processData(data));
            //         this.set('isLoading', false);
            //     }.bind(this)
            // );
        },

        // Delete an option in selected pool
        async deleteSelectedDataById(id) {
            if (!Array.isArray(id)) id = [id];

            this.set('selectedData', 
                await this.get('selectedData').filter(function(node) { return (id.indexOf(node.id) != -1) ? false : true; })
            );
            this.send('updateSelectedBox');
        },

        // Actions that helps trigger the detail panel when
        // user hovers to an option
        viewNodeDetail(node, position = 2) {
            try {
                let newNode = Ember.copy(node);
                newNode.audience_size_str = newNode.audience_size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                this.set('isDescriptionPanelVisible', newNode);
                this.set('descriptionPanelPosition', position || 2);
            } catch (e) { return; }
        },
        hideNodeDetail() {
            this.set('isDescriptionPanelVisible', false);
        },

        // Action that updates selected options box data
        async updateSelectedBox(newData) {
            await this.set('processedSelectedData', await this.processSelectedData(this.get('selectedData')));

            // Blink the newly added
            // only blink if newly added 
            if (typeof(newData) !== 'undefined') {
                Ember.run.later(() => {
                    let selector = this.$("#am-selectbox-selected-id-" + newData.id);
                    selector.stop().hide().fadeIn(800);
                    this.$('.am-selectbox-selected').scrollTop(0);

                    Ember.run.later(() => this.$('.am-selectbox-selected').animate({
                        scrollTop: selector.position().top - 30
                    }, 'fast'), 50);
                }, 30);
            }
        },

        removeSelectedOptionBox(type, data) {
            switch (type) {
                case 'path': {
                    let idsToDelete = [];
                    this.get('processedSelectedData').forEach(pathParent => {
                        if (JSON.stringify(pathParent.path) === JSON.stringify(data)) {
                            pathParent.options.forEach(node => {
                                idsToDelete.push(node.id);
                            });
                        }

                        this.send('deleteSelectedDataById', idsToDelete);
                    });
                    break;
                }

                case 'option': {
                    this.send('deleteSelectedDataById', data);
                    break;
                }
            }
        }
    },

    // Helpers functions
    async processData(data) {
        let nodes = {};

        // A function that create object keys based on path
        let createNodePath = (object, paths, level = 0) => {
            if (typeof(object[paths[level]]) === 'undefined') {
                object[paths[level]] = {};
            }

            // If visit the last level, return the reference
            if (level + 1 > paths.length - 1) {
                return object[paths[level]];
            }
            return createNodePath(object[paths[level]], paths, level + 1);
        }

        // A function that convert path objects to arrays
        // that can be resolved by handlebars
        let objToArray = (object) => {
            let array = [];

            // If this is the last level (a clickable property), stop recursion
            if (typeof(object.type) !== 'undefined' || typeof(object.audience_size) !== 'undefined') {
                return object;
            }

            Object.keys(object).map((node) => {
                array.push({
                    name: node,
                    items: objToArray(object[node])
                });
            });

            return array;
        }

        // Loop through the data itself to populate the path objects.
        data.map((node) => {
            let lastLevel = nodes;
            if (Array.isArray(node.path)) {
                lastLevel = createNodePath(nodes, node.path, 0);
            }

            if (typeof(node.type) !== 'undefined' || typeof(node.audience_size) !== 'undefined') {
                lastLevel[node.id] = node;
            }
        });

        return objToArray(nodes);
    },

    async processSelectedData(data) {
        var selectedBoxData = {};

        let pathToString = paths => {
            return paths.join(' > ');
        };

        data.map((option) => {
            let pathKey = pathToString(option.path);

            // Create a path, if it is not available
            if (typeof(selectedBoxData[pathKey]) === 'undefined') {
                selectedBoxData[pathKey] = {
                    path: option.path,
                    paths: [],
                    options: []    
                };
            }

            // Insert a new option into this path 
            if (!selectedBoxData[pathKey].options.includes(option)) {
                selectedBoxData[pathKey].options.push(option);
            }

            // Create paths for child paths 
            for (let i = 0; i < option.path.length; i++) {
                selectedBoxData[pathKey].paths[i] = option.path.slice(0, i + 1);
            }
        });

        // Sort the paths
        let unorderedSelectedBoxData = Ember.copy(selectedBoxData);
        selectedBoxData = [];
        Object.keys(unorderedSelectedBoxData).sort().forEach(function(path) {
            selectedBoxData.push(unorderedSelectedBoxData[path]);
        });

        // If the paths are empty, clear this object
        if (Object.keys(selectedBoxData).length === 0) {
            selectedBoxData = false;
        }

        return selectedBoxData;
    },

    checkSelected(data) {
        if (this.get('selectedData') == false || this.get('selectedData').length === 0) {
            return false;
        } else {
            let i = 0;

            this.get('selectedData').forEach(option => {
                if (parseInt(option.id) == parseInt(data.id)) {
                    i++;
                }
            });

            return (i > 0) ? true : false;
        }
    }
});
