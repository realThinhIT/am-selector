import Ember from 'ember';

export default Ember.Route.extend({
    selectedData: [],

    model() {
        return {
            selectedData: this.get('selectedData')
        }
    },
    
    actions: {
        selectBoxUpdate(searchQuery, type, callback) {
            switch (type) {
                case 'suggestions': {
                    return Ember.$.getJSON('/json/data_suggestions.json').then(res => {
                        return callback(null, res);
                    });
                }

                case 'browse': {
                    return Ember.$.getJSON('/json/data_browse.json').then(res => {
                        return callback(null, res);
                    });
                }

                case 'suggestions_untab': {
                    return Ember.$.getJSON('/json/data_after_search.json').then(res => {
                        return callback(null, res.data);
                    });
                }
            }
        }
    }
});
